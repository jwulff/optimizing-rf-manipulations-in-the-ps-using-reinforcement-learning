# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 13:06:12 2021

@author: Joel Wulff
"""
import numpy as np
from scipy import fftpack
import numpy as np
import imageio
import matplotlib.pyplot as plt
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate

from simulation import Simulation

lookup_table = np.load('lookup_table_bunch_lengths_tri.npy')
phase = np.linspace(-15,15,31)
volt = np.linspace(0.9,1.1,11)

points = (phase, phase, volt)
values1 = lookup_table[:,:,:,0]


def interpolation(x, y, z, phase=phase, volt=volt, lookup_table=lookup_table):  # Interpolating function
    values1 = lookup_table[:,:,:,0]
    values2 = lookup_table[:,:,:,1]
    values3 = lookup_table[:,:,:,2]
    
    return_array = interpolate.interpn(points, values1, np.array([x, y, z]))
    return_array = np.append(return_array, interpolate.interpn(points, values2, np.array([x, y, z])))
    return_array = np.append(return_array, interpolate.interpn(points, values3, np.array([x, y, z])))
    return return_array

print(interpolation(0,0,1.0))
print(interpolation(14.5,14.5,1.0))
print(interpolation(14,14,1.0))
p1 = 0
p2 = 1
v14=1.05
print(interpolation(p1,p2,v14))
observable = interpolation(p1,p2,v14)
#%% Diff estimate testing

diff_estimate = 0
for bunch1 in observable:
    for bunch2 in observable:
        diff_estimate += abs(bunch1 - bunch2)
diff_estimate = diff_estimate
print(diff_estimate)
#for p1 in p42:
#    for p2 in p84:
#        extended_matrix[np.where(p42==p1), np.where(p84==p2)] = spline.__call__(p1 + 15, p2+15)
        
#plt.imshow(extended_matrix)