# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:31:52 2021

@author: Joel Wulff
"""
from scipy import fftpack
import numpy as np
import imageio
import matplotlib.pyplot as plt
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate as interp

from simulation import Simulation

def objective_function(initial_phase_errors, splitting='double'):
    
    sim = Simulation(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.process_data()

def simulate_bunch_lengths(initial_phase_errors, splitting='double'):
    
    sim = Simulation(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.get_datamatrix()

def callbackF(xk):
    global Neval
    print("Iterations: {}  Current pos {}".format(Neval, xk))
    Neval += 1
    return
    
def optimization_lookup(initial_phase_errors, lookup_table, scaling=False):
    rows, cols = np.shape(lookup_table)
    spline = interp.RectBivariateSpline(np.arange(0,rows), 
                                        np.arange(0,cols), 
                                        lookup_table)
    # print(lookup_table[5,5])
    # print(lookup_table[5,6])
    # test = spline.__call__(5.5, 5.6)
    
    # Translate phase errors to indexes
    p42 = initial_phase_errors[0] + 45
    p84 = initial_phase_errors[1] + 45
    print("p42 idx {}, and p84 idx {}".format(p42, p84))
    result = spline.__call__(p42, p84)[0][0]
    print(result)
    return result
    
    
def test_optimizer_lookup(produce_plots = True):
    lookup_table = np.load('lookup_table.npy')
    initial_phase_errors = np.array([-30, 30])
    
    x1 = initial_phase_errors + np.array([10, 0])
    x2 = initial_phase_errors + np.array([0, 10])
    initial_simplex = [initial_phase_errors, x1, x2]
    
    res_nm = opt.minimize(optimization_lookup, initial_phase_errors, 
                       args=(lookup_table), method = 'Nelder-Mead', 
                       callback=callbackF, 
                       bounds=[[-45,45], [-45,45]], 
                       options= {'disp': True, 
                                 'initial_simplex': initial_simplex,
                                 'return_all': True})
    res_p = opt.minimize(optimization_lookup, initial_phase_errors, 
                       args=(lookup_table), method = 'Powell', 
                       callback=callbackF, 
                       bounds=[[-45,45], [-45,45]], 
                       options= {'disp': True,
                                 'return_all': True})
    print(res_nm)
    correction = res_nm['x'] - initial_phase_errors
    correction_p = res_p['x'] - initial_phase_errors
    print("Nelder-Mead: Adjust offsets by: p42: {}, p84 {}".format(correction[0], correction[1]))
    print("Powell: Adjust offsets by: p42: {}, p84 {}".format(correction_p[0], correction_p[1]))
    print("Correct adjustments: {}".format(-initial_phase_errors))
    if produce_plots:
        # make plot
        fig, ax = plt.subplots()
          
        # show image
        shw = ax.imshow(lookup_table, extent=[-45,45, 45, -45])
          
        # make bar
        bar = plt.colorbar(shw)
          
        # show plot with labels
        plt.xlabel('phi_84')
        plt.ylabel('phi_42')
        x_opt = []
        y_opt = []
        x_opt_p = []
        y_opt_p = []
        for point in res_nm['allvecs']:
            x_opt.append(point[0])
            y_opt.append(point[1])
        plt.plot(x_opt, y_opt, 'bo-', label = 'Nelder-Mead')
        for point in res_p['allvecs']:
            x_opt_p.append(point[0])
            y_opt_p.append(point[1])
        plt.plot(x_opt_p,y_opt_p, 'rx-', label = 'Powell')
        bar.set_label('Normalized obj function')
        plt.legend()
        plt.show()



Neval = 1


def testfunction(x):
    return x**2

def test_optimizer(opt_param = 'length'):
    if opt_param == 'length':
        x0 = 10 # Start position
        res = opt.minimize(testfunction, x0, method='Nelder-Mead', callback=callbackF)
        print("Success: {}, Solution {}, Nbr of iterations {}".format(res.success, res.x, res.nit))
    return res

def optimizer(opt_param = 'length'):
    if opt_param == 'length':
        x0 = np.array([10, -10]) # Start position
        res = opt.minimize(objective_function, x0, method='Nelder-Mead', callback=callbackF)
        print("Success: {}, Solution {}, Nbr of iterations {}".format(res.success, res.x, res.nit))
    return None

  # Value of sum of np.abs(fft(bunchLengths))
                                # for the two peaks in freq. spectrum
def f(x):
    return objective_function(x)

def create_new_lookup_table():
    H84_range = range(-10,10)
    H42_range = range(-10,10)
    table = np.zeros((abs(H42_range.stop-H42_range.start)+1,
                      abs(H84_range.stop-H84_range.start)+1))
    for i, phase_42 in enumerate(H42_range):
        for j, phase_84 in enumerate(H84_range):
            sum_mod_error = objective_function([phase_42, phase_84])
            table[i][j] = sum_mod_error
    

def test_one_run():
    output = objective_function([-45, 45])
    print("Output value {}".format(output))

    

def testing():
    H84_range = range(-10,11)
    H42_range = range(-10,11)
    input_phases_84 = np.empty(0)
    input_phases_42  = np.empty(0)
    table = np.zeros((abs(H42_range.stop-H42_range.start),
                      abs(H84_range.stop-H84_range.start)))
    for i, phase_42 in enumerate(H42_range):
        input_phases_42 = np.append(input_phases_42, phase_42)
        for j, phase_84 in enumerate(H84_range):
            print("Phase 42 nbr: {}/{}, phase 84 nbr {}/{}".format(i, len(H42_range),
                                                                   j, len(H84_range)))
            sum_mod_error = objective_function([phase_42, phase_84])
            table[i][j] = sum_mod_error
            input_phases_84 = np.append(input_phases_84, phase_84)
           
    np.save('input_phases_84', input_phases_84)
    np.save('input_phases_42', input_phases_42)
    np.save('lookup_table_3', table)
    

def run_sim():
    sim = Simulation()
    phase_offsets = optimizer(sim.run_sim())
    

if __name__ == "__main__":
    start = perf_counter()
    #run_sim()
    #res = objective_function([0,0])
    #print('Result {}'.format(res))
    #test_one_run()
    #test_optimizer_lookup()
    datamatrix, profile, rf_station = simulate_bunch_lengths([0,0])
    plt.figure()
    plt.imshow(datamatrix, aspect='auto', origin='lower')
    plt.xlim((0, 1000))

    plt.figure()
    plt.plot(profile.bin_centers, datamatrix[400,:])
    plt.xlim((0, rf_station.t_rf[0,0]))
    stop = perf_counter()
    print('The script took %f s to run.' % (stop-start))