# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 13:06:12 2021

@author: Joel Wulff
"""
import numpy as np
from scipy import fftpack
import numpy as np
import imageio
import matplotlib.pyplot as plt
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate as interp

from simulation import Simulation

lookup_table = np.load('lookup_table.npy')
rows, cols = np.shape(lookup_table)
spline = interp.RectBivariateSpline(np.arange(0,rows), 
                                    np.arange(0,cols), 
                                    lookup_table)
extended_matrix = np.zeros((rows*2-1, cols*2-1))

p42 = np.linspace(-15,15,61)
p84 = np.linspace(-15,15,61)

for p1 in p42:
    for p2 in p84:
        extended_matrix[np.where(p42==p1), np.where(p84==p2)] = spline.__call__(p1 + 15, p2+15)
        
plt.imshow(extended_matrix)