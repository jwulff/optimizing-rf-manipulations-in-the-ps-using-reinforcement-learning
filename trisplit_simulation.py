# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 10:31:11 2021

@author: Joel Wulff
"""

# General imports
import scipy as scp
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib widget
import time
import datetime
from scipy import fftpack
import blond

# BLonD imports
from blond.input_parameters.ring import Ring, RingOptions
from blond.input_parameters.rf_parameters import RFStation
from blond.beam.beam import Proton, Beam
from blond.beam.profile import Profile, CutOptions
from blond.trackers.tracker import RingAndRFTracker, FullRingAndRF
# from blond.impedances.impedance_sources import Resonators, InputTable
# from blond.impedances.impedance import InducedVoltageFreq, InductiveImpedance, TotalInducedVoltage
from blond.beam.distributions_multibunch import match_beam_from_distribution


class TrisplitSimulation():
    
    def __init__(self, initial_phase_errors = None, v14_factor=1., n_bunches=1, 
                 n_slices_per_bunch=2**10, n_rf_systems=3, n_macroparticles_per_bunch = int(1e4), split_type = 2):
        """
        Constructor
        """
        
        # Simulation parameters
        self.plot_turn = 5000
        time_sim_start = 1827e-3
        time_sim_end = 1890e-3
        
        # General parameters
        circumference = 2.*np.pi*100.                    # Machine circumference [m]
        gamma_transition = 6.1                           # Transition gamma
        momentum_compaction = 1./gamma_transition**2  # Momentum compaction array
        particle_type = Proton()
        ramp_interpolation = 'linear'
        loaded_momentum = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/ramp.npz')
        time_momentum = loaded_momentum['time']*1e-3        # Time [s] 
        momentum = loaded_momentum['momentum']*1e9          # Momentum [eV/c]
        
        #plt.figure()
        #plt.plot(time_momentum, momentum)
        
        # RF parameters 

        v14_factor = 1.
        
        n_rf_systems = 3                                         # Number of rf systems 
        harmonic_numbers = [7, 14, 21]                          # Harmonic numbers
        loaded_h7 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C10_h-7.npz')
        time_h7 = loaded_h7['time']*1e-3                          # Time [s]
        voltage_h7 = loaded_h7['amplitude']*1e3                   # Voltage [V]
        loaded_h14 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C10_h-14.npz')
        time_h14 = loaded_h14['time']*1e-3                          # Time [s]
        voltage_h14 = loaded_h14['amplitude']*1e3  * v14_factor                 # Voltage [V]
        loaded_h21 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C10_h-21.npz')
        time_h21 = loaded_h21['time']*1e-3                          # Time [s]
        voltage_h21 = loaded_h21['amplitude']*1e3                   # Voltage [V]
        
        #plt.figure()
        #plt.plot(time_h7, voltage_h7)
        #plt.plot(time_h14, voltage_h14)
        #plt.plot(time_h21, voltage_h21)
        #plt.xlim((time_sim_start,time_sim_end))
        #plt.ylim((0, 1.1*voltage_h21[time_h21<=time_sim_end][-1]))
        
        # Phase offsets, the possible errors
        dpc14 = initial_phase_errors[0]
        dpc21 = initial_phase_errors[1]
        phi_offsets =  [np.pi, np.pi+dpc14/180*np.pi, np.pi+dpc21/180*np.pi]     # For 2x2 splitting
        
        # Beam parameters
        n_bunches = 1
        
        initial_bunch_length_full = 155e-9
        initial_exponent = 1.
        
        n_macroparticles_per_bunch = int(1e4)
        intensity_per_bunch = 1.3e11
        
        intensity = intensity_per_bunch*n_bunches*3*4
        n_macroparticles = int(n_macroparticles_per_bunch*n_bunches*4)
        
        # Profile parameters
        n_slices_per_bunch = 2**10
        self.bunch_spacing_buckets = 1
        n_slices = int(n_slices_per_bunch*(self.bunch_spacing_buckets*(harmonic_numbers[0]-1)+1))
        cut_left = 0.
        cut_right = 2*np.pi*(self.bunch_spacing_buckets*(harmonic_numbers[0]-1)+1)
        
        ### Ring object

        momentum = momentum[(time_momentum>=time_sim_start-2e-3)*(time_momentum<=time_sim_end+2e-3)]
        time_momentum = time_momentum[(time_momentum>=time_sim_start-2e-3)*(time_momentum<=time_sim_end+2e-3)]
        
        
        ring_options = RingOptions(interpolation=ramp_interpolation,
                                      t_start=time_sim_start,
                                      t_end=time_sim_end)
        self.ring = Ring(circumference, momentum_compaction,
                       (time_momentum, momentum), particle_type,
                       RingOptions=ring_options)
        ### RFStation object

        voltage_h7 = voltage_h7[(time_h7>=time_sim_start)*(time_h7<=time_sim_end)]
        time_h7 = time_h7[(time_h7>=time_sim_start)*(time_h7<=time_sim_end)]
        
        voltage_h14 = voltage_h14[(time_h14>=time_sim_start)*(time_h14<=time_sim_end)]
        time_h14 = time_h14[(time_h14>=time_sim_start)*(time_h14<=time_sim_end)]
        
        voltage_h21 = voltage_h21[(time_h21>=time_sim_start)*(time_h21<=time_sim_end)]
        time_h21 = time_h21[(time_h21>=time_sim_start)*(time_h21<=time_sim_end)]
        
        self.rf_station = RFStation(self.ring, harmonic_numbers,
                               ((time_h7, voltage_h7),
                                (time_h14, voltage_h14),
                                (time_h21, voltage_h21)),
                               phi_offsets,
                               n_rf=n_rf_systems)
        ### Beam
        self.beam = Beam(self.ring, n_macroparticles, intensity)
        
        
        ### Profile
        cut_options = CutOptions(cut_left, cut_right, n_slices=n_slices,
                                 cuts_unit='rad', RFSectionParameters=self.rf_station)
        self.profile = Profile(self.beam, CutOptions=cut_options)
        
        ### Tracker

        longitudinal_tracker = RingAndRFTracker(self.rf_station, self.beam)
        
        self.full_tracker = FullRingAndRF([longitudinal_tracker])
        
        
        ### Beam generation
        distribution_options = {'type': 'binomial', 'exponent': initial_exponent,
                                'bunch_length': initial_bunch_length_full,
                                'bunch_length_fit': 'full', 
                                'density_variable': 'Hamiltonian'}
        
        match_beam_from_distribution(self.beam, self.full_tracker, self.ring,
                                     distribution_options, n_bunches,
                                     self.bunch_spacing_buckets,
                                     n_points_potential=int(1e3))
        
        self.profile.track()
        #plt.figure()
        #plt.plot(self.profile.bin_centers, self.profile.n_macroparticles)
        
        #plt.figure()
        #plt.plot(self.beam.dt, self.beam.dE, '.')
        
        
    def run_sim(self):
        # Move the bunches in the center of the turn
        #self.beam.dt += self.rf_station.t_rf[0,0]
        
        self.datamatrix = np.zeros((int(self.ring.n_turns/100)+1, len(self.profile.n_macroparticles)))
        
        for turn in range(self.ring.n_turns):
        
            if(turn%(self.plot_turn)==0):
                t0 = time.perf_counter()
                
            if (turn%100==0):
                self.datamatrix[int(turn/100), :] = self.profile.n_macroparticles
        
            # Track
            self.full_tracker.track()
            self.profile.track()
            
        
            if(turn%self.plot_turn==0):
                t1 = time.perf_counter()
                print('Turn %d over %d, ETC: %s' %(
                    turn, self.ring.n_turns, datetime.timedelta(seconds=(t1-t0)*(self.ring.n_turns-turn))))
        self.turn = turn # Extract final turn for analysis
        #plt.figure()
        #plt.plot(self.profile.bin_centers, self.profile.n_macroparticles)
        #plt.xlim((0, self.rf_station.t_rf[0,0]))
        #plt.title('Triple split')
        #plt.ylabel('Nbr of particles')
        #plt.xlabel('time (s)')
        #plt.figure()
        #plt.imshow(self.datamatrix, aspect='auto', origin='lower')
        #plt.title('Triple split')
        #plt.ylabel('turn/100')
        #plt.xlabel('bin')
        #plt.xlim((0, 1000))

    def bunch_length(self):
        bunches_after_split = 3
        bucket_size_tau = self.rf_station.t_rf[self.rf_station.n_rf-1,self.turn] # At the end, only the last harmonic is active
        #print(bucket_size_tau)
        self.profile.rms_multibunch(bunches_after_split, self.bunch_spacing_buckets, bucket_size_tau, bucket_tolerance=0)
        # Bunch lengths collected in self.profile.bunchLength
        return self.profile.bunchLength