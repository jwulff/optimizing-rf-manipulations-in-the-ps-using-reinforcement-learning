from stable_baselines3.common.env_checker import check_env
import gym
import numpy as np
#from quadsplit_class_continuous import QuadSplitContinuous
from twosplit_class_bunch_lengths import TwoSplitContinuousBL
from quadsplit_class_bunch_lengths import QuadSplitContinuousBL
from trisplit_class_bunch_lengths import TriSplitContinuousBL

env = TriSplitContinuousBL()
result = check_env(env)