# -*- coding: utf-8 -*-
"""
Class defining the possible actions for the agent to do and the corresponding
reward
"""

#%% Imports

from quadsplit_class_bunch_lengths import BUNCH_LENGTH_CRITERIA
import random
import numpy as np
import gym
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
from scipy import interpolate
#from plots.enhancer import plot_finish


#%% Twosplit environment class

BUNCH_LENGTH_CRITERIA = 0.002
#OFFSET = 5

REWARD_OFFSET = -1

REWARD_FUNC = 'gauss' 
#REWARD_FUNC = 'parabola'
#REWARD_FUNC = 'observable'
#REWARD_FUNC = 'simple'

ACTION_TYPE = 'relative'
#ACTION_TYPE = 'absolute'

# Loading simulation data
loaded_results = np.load('..\lookup_table_bunch_lengths.npy')
phase = np.linspace(-45,45,181) #loaded_results['dpc20'] + OFFSET
bunch_lengths = loaded_results # Bunch lengths are normalized between 0 and 1.

function = interpolate.interp1d(phase, loaded_results, axis=0) # Interpolating function

min_setting=np.min(phase)
max_setting=np.max(phase)
min_spread=np.min(bunch_lengths)
max_spread=np.max(bunch_lengths)

# Normalize peak_mod_int

#peak_mod_int = peak_mod_int/max_spread


class TwoSplitContinuousBL(gym.Env):
    """
    Environment class for the foursplit optimization (action and reward)
    """
    
    def __init__(self, max_step_size=45,
                 min_setting=min_setting, max_setting=max_setting,
                 min_spread=min_spread, max_spread=max_spread, max_steps=100,
                 seed=None, interp_function = function):
        """
        Initialize the environment
        """
        
        ### Settings
        self.min_setting = min_setting
        self.max_setting = max_setting
        self.phase_set = 0
        
        ### Define what the agent can do
        # Does not seem to work...
        self.max_step_size = max_step_size
        if ACTION_TYPE == 'relative':
            self.action_space = gym.spaces.Box(
                    -1,
                    1,
                    shape=(1,),
                    dtype=np.float32)
        elif ACTION_TYPE == 'absolute': # NOT USED IN ANY FINAL MODELS
            self.action_space = gym.spaces.Box(
                    self.min_setting,
                    self.max_setting,
                    shape=(1,),
                    dtype=np.float32)
        
        ### Observable/State
        self.min_spread = min_spread
        self.max_spread = max_spread
        self.interp_function = interp_function
        self.state = [0, 0]
        self.action = 0
        
        ### Define what the observations are to be expected
        self.observation_space = gym.spaces.Box(
                np.array([0, 0]),
                np.array([1, 1]),
                shape=(2,),
                dtype=np.float32)

        ### Apply the simulated data to the environment to get
        # observables vs. settings
        self.setting_sim = np.array(phase)
        self.bunch_length_1 = np.array(bunch_lengths[:,0])
        self.bunch_length_2 = np.array(bunch_lengths[:,1])
        #self.observable_sim_extended = np.polynomial.polynomial.Polynomial.fit(self.setting_sim, self.observable_sim, 2)
        self.reward_width = 0.25
        # Best rewards for DDPG/NAF
        
        if REWARD_FUNC == 'gauss':
            pass
            #self.reward_sim = np.exp(-(self.observable_sim/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
        elif REWARD_FUNC == 'parabola':
            self.reward_sim = 1-(self.observable_sim/(3*self.reward_width))**2+REWARD_OFFSET
        elif REWARD_FUNC == 'linear':
            self.reward_sim = np.abs(self.observable_sim) + REWARD_OFFSET
        #elif REWARD_FUNC == 'simple':
        #    self.reward_sim = -self.observable_sim_extended(self.setting_sim) + REWARD_OFFSET
    
        ### Status of the iterations
        # Steps, i.e. number of cycles
        self.counter = 0
        self.curr_step = -1  ## Not used ?
        self.max_steps = max_steps
        
        # Episodes, i.e. number of MDs, number of LHC fills...
        self.curr_episode = -1
        self.action_episode_memory = []
        self.state_memory = []
        self.phase_set_memory = []
        self.reward_memory = []
        self.is_finalized = False
        

        ### Eval
        self.initial_offset = None
        self.phase_correction = 0.0

        ### Set the seed
        self.seed(seed)
        
        ### Reset
        #self.reset()
        
        
    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward
        """
        success = False    
        self.curr_step += 1
        #print(f" state before action {self.state}, action {action}")
        self._take_action(action)
        #print(f" state After action {self.state}")
        reward = self._get_reward()
        state = self.state
        if abs(state[1]-state[0]) < 0.002:
            self.is_finalized = True
            success = True
            #reward = reward + 1 # Reward early succesful optimization?
        if self.counter >= self.max_steps:
            self.is_finalized = True

            ### Don't allow it to go too far from the data
        #if (self.phase_set<self.min_setting-20) or (self.phase_set>self.max_setting+20):
        #    self.is_finalized = True
        #reward = reward + state[1] # Add bonus if observable shrinks, minus otherwise
        self.reward_memory[self.curr_episode].append(reward)
        info = {'success': success, 'steps': self.counter, 'phase_corr': self.phase_correction, 'initial_phase': self.initial_offset}
        
        return state, reward, self.is_finalized, info
    
    def _take_action(self, action):
        """
        Actual action funtion
        """
        
        action = action[0]
        self.action = action
        converted_action = action*self.max_step_size
        self.phase_correction += converted_action
        # Phase offset as action, Best action for DDPG
        if ACTION_TYPE == 'relative':
            self.phase_set += converted_action 
        
        # Absolute phase as action, Best action for NAF
        elif ACTION_TYPE == 'absolute':
            self.phase_set = converted_action
    
        self.state = self._get_state()
        
        self.action_episode_memory[self.curr_episode].append(action)
        self.state_memory[self.curr_episode].append(self.state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        self.counter += 1
    
    def _get_state(self):
        '''
        Get the observable for a given phase_set
        '''

        if (self.phase_set<self.min_setting):
            state = np.array([1.0, 0.5])
        elif (self.phase_set>self.max_setting):
            state = np.array([0.5,1.0])
        else:
            # Interpolating the state/observable from the simulated data
            state = function(self.phase_set)
        
        return state
    
    def _get_reward(self):
        '''
        Evaluating the reward from the observable
        '''
        
        observable = self.state
        # Best rewards for DDPG/NAF
        if REWARD_FUNC == 'observable':
            reward = -self.observable_sim_extended(self.phase_set)# + 0.25
        if REWARD_FUNC == 'gauss':
            difference = abs(observable[1]-observable[0])
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = np.exp(-(difference/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            elif abs(observable[1]-observable[0]) < BUNCH_LENGTH_CRITERIA:
                reward = 10
            else:
                reward = np.exp(-(difference/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
                
        elif REWARD_FUNC == 'parabola':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = 1-(observable/(3*self.reward_width))**2 - 1 + REWARD_OFFSET
            else:
                reward = 1-(observable/(3*self.reward_width))**2 + REWARD_OFFSET
        
        elif REWARD_FUNC == 'linear':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = -1 + REWARD_OFFSET
            else:
                reward = -np.abs(np.linspace(-45,45,91))/45 + REWARD_OFFSET
        elif REWARD_FUNC == 'simple':
            reward = -1
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward += -4
            
            elif abs(observable[1]-observable[0]) < BUNCH_LENGTH_CRITERIA:
                reward += 101
            elif abs(observable[1]-observable[0]) < 0.01:
                reward += 0.5
        return reward
    
    def reset(self):
        """
        Reset to a random state to start over the training
        """
        
        # Resetting to start a new episode
        self.curr_episode += 1
        self.counter = 0
        self.is_finalized = False

        # Initializing list to save steps for that episode
        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])
        
        # Getting initial state
        self.phase_set = random.uniform(self.min_setting,
                                        self.max_setting)
        #self.prev_state = self.phase_set + 5
        self.initial_offset = np.copy(self.phase_set)
        self.phase_correction = 0.0
        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)
        
#        if self.curr_episode == 2:
#            plt.figure('Observable')
#            plt.savefig('observable_init.png')
#            plt.figure('Reward')
#            plt.savefig('reward_init.png')
            
        

        return state

    def set_state(self, phase):
        self.phase_set = phase
        self.initial_offset = np.copy(self.phase_set)

        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])

        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)

        return state

    def seed(self, seed=None):
        """
        Set the random seed
        """
        
        random.seed(seed)
        np.random.seed
        
    def render(self, mode='human'):
        
        

        plt.figure('Observable')
        plt.clf()
        plt.plot(self.setting_sim, self.bunch_length_1, 'b')
        plt.plot(self.setting_sim, self.bunch_length_2, 'r')
        bunch_lengths_1 = np.zeros(len(self.state_memory[self.curr_episode]))
        for i, state in enumerate(self.state_memory[self.curr_episode]):
            bunch_lengths_1[i] = state[0]
        plt.plot(self.phase_set_memory[self.curr_episode], bunch_lengths_1, 'ok')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], bunch_lengths_1[-1], 'or')
        bunch_lengths_2 = np.zeros(len(self.state_memory[self.curr_episode]))
        for i, state in enumerate(self.state_memory[self.curr_episode]):
            bunch_lengths_2[i] = state[1]
        plt.plot(self.phase_set_memory[self.curr_episode], bunch_lengths_2, 'og')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], bunch_lengths_2[-1], 'ob')
        fig = plt.gcf()
        axes = plt.gca()
        
        #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
        plt.pause(0.1)
        
        plt.figure('Reward')
        plt.clf()
        #plt.plot(self.setting_sim, -self.observable_sim, 'b')
        
        #plt.plot(self.setting_sim, self.reward_sim, 'b')
        plt.plot(self.phase_set_memory[self.curr_episode], self.reward_memory[self.curr_episode], 'ok')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], self.reward_memory[self.curr_episode][-1], 'or')
        fig = plt.gcf()
        axes = plt.gca()
      #  plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Reward')
        plt.pause(0.1)
        
        
        
        







