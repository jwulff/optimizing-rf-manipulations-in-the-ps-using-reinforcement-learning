#%%
import gym
import numpy as np
from twosplit_class_continuous_two_obs import TwoSplitContinuous
from twosplit_class_bunch_lengths import TwoSplitContinuousBL
from stable_baselines3 import SAC
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.callbacks import EvalCallback
from scipy import interpolate


model = SAC.load("./logs/twosplit/report_models/sac-simple-auto")
env = TwoSplitContinuousBL()

obs = env.reset()
performance = [0,0]
steps_per_episode = []
predicted_phase_error_when_done = []
for _ in range(500):
    #print(obs)
    action, _states = model.predict(obs)
    #print(action*10)
    obs, rewards, done, info = env.step(action)
    #print(obs)
    #env.render() # Use if you want to observe the agent
    if done:
        print(f"Took {info['steps']} steps before terminating test")
        print(f"Info {info}")
        steps_per_episode.append(info['steps'])
        if info['success'] == True:
            performance[0] += 1
        else:
            performance[1] += 1
        predicted_phase_error_when_done.append(info['initial_phase'] + info['phase_corr'])
        obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")
print(f"Mean episode length: {np.sum(steps_per_episode)/len(steps_per_episode)}")
print(f"Max episode length: {np.max(steps_per_episode)}, Min episode length: {np.min(steps_per_episode)}")

p42_err = []
plt.hist(predicted_phase_error_when_done, 91, (-20,20))
plt.title('TD3-Gauss: Histogram of end prediction errors')
plt.xlabel('Phase correction error')
plt.ylabel('Number of episodes')
plt.show()

# Loading simulation data
loaded_results = np.load('..\lookup_table_bunch_lengths.npy')
phases = np.linspace(-45,45,181) #loaded_results['dpc20'] + OFFSET
bunch_lengths = loaded_results # Bunch lengths are normalized between 0 and 1.
idx = np.linspace(0,180,181)
function = interpolate.interp1d(phases, loaded_results, axis=0) # Interpolating function

phase_errors = np.zeros(len(phases))

for i, phase in enumerate(phases):
    mean_phase_error = 0
    for j in range(10):
        first_guess = True
        env.reset()
        env.set_state(phase)
        done = False
        while not done:
            if first_guess:
                action, _states = model.predict(function(phase))
                first_guess = False
            else:
                action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
        mean_phase_error += info['initial_phase'] + info['phase_corr']
    mean_phase_error = mean_phase_error/10
    phase_errors[i] = mean_phase_error

plt.figure()
plt.plot(phases, phase_errors)
plt.xlabel('Initial phase offset')
plt.ylabel('Prediction error')
plt.ylim([-5,5])
plt.title('TD3-Gauss: Error in phase prediction for different initial settings')
plt.show()