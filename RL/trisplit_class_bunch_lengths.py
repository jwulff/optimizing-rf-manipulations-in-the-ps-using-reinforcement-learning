# -*- coding: utf-8 -*-
"""
Class defining the possible actions for the agent to do and the corresponding
reward
"""

#%% Imports

import random
import numpy as np
import gym
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
from numpy.lib.function_base import diff
from scipy import interpolate
#from plots.enhancer import plot_finish


#%% Twosplit environment class

OFFSET = 5

REWARD_OFFSET = -1

BUNCH_LENGTH_CRITERIA = 0.08 # Empirically evaluated diff_estimate that constitutes a "good" bunch splitting. Lower means longer training time, but smaller spread in bunch lengths.
#SIMULATION_DATA = 'corrected'
SIMULATION_DATA = 'uncorrected'

#REWARD_FUNC = 'gauss' 
#REWARD_FUNC = 'parabola'
#REWARD_FUNC = 'observable'
REWARD_FUNC = 'simple'

ACTION_TYPE = 'relative'
#ACTION_TYPE = 'absolute'

# Loading simulation data

loaded_results = np.load('..\lookup_table_bunch_lengths_tri.npy')


phase = np.linspace(-15,15,31) #loaded_results['dpc20'] + OFFSET
volt = np.linspace(0.9,1.1,11)
points = (phase, phase, volt)

bunch_lengths = loaded_results # Bunch lengths are normalized between 0 and 1.


def interpolation(x, y, z, points = points, lookup_table=loaded_results):  # Interpolating function
    values1 = lookup_table[:,:,:,0]
    values2 = lookup_table[:,:,:,1]
    values3 = lookup_table[:,:,:,2]
    
    return_array = interpolate.interpn(points, values1, np.array([x, y, z]))
    return_array = np.append(return_array, interpolate.interpn(points, values2, np.array([x, y, z])))
    return_array = np.append(return_array, interpolate.interpn(points, values3, np.array([x, y, z])))
    return return_array

min_setting=np.min(phase)
max_setting=np.max(phase)
min_spread=np.min(bunch_lengths)
max_spread=np.max(bunch_lengths)

# Normalize peak_mod_int

#peak_mod_int = peak_mod_int/max_spread


class TriSplitContinuousBL(gym.Env):
    """
    Environment class for the foursplit optimization (action and reward)
    """
    
    def __init__(self, max_step_size=15,
                 min_setting=min_setting, max_setting=max_setting,
                 min_spread=min_spread, max_spread=max_spread, max_steps=100,
                 seed=None):
        """
        Initialize the environment
        """
        
        ### Settings
        self.min_setting = min_setting
        self.max_setting = max_setting
        self.phase_volt_set = [0.0,0.0,0.0]
        self.max_step_volt = 0.1
        
        ### Define what the agent can do
        # Does not seem to work...
        self.max_step_size = max_step_size
        if ACTION_TYPE == 'relative':
            self.action_space = gym.spaces.Box(
                    np.array([-1, -1, -1]),
                    np.array([1, 1, 1]),
                    shape=(3,),
                    dtype=np.float32)
        elif ACTION_TYPE == 'absolute':
            self.action_space = gym.spaces.Box(
                    self.min_setting,
                    self.max_setting,
                    shape=(1,),
                    dtype=np.float32)
        
        ### Observable/State
        self.min_spread = min_spread
        self.max_spread = max_spread
        self.state = [0, 0]
        self.action = 0
        self.diff_estimate = None

        ### Evaluation info
        self.phase_correction = 0
        self.initial_offset = 0

        
        ### Define what the observations are to be expected
        self.observation_space = gym.spaces.Box(
                np.array([0, 0, 0]),
                np.array([1, 1, 1]),
                shape=(3,),
                dtype=np.float32)

        ### Apply the simulated data to the environment to get
        # observables vs. settings
        self.setting_sim = np.array(phase)
        #observables = interpolation(self.setting_sim, self.setting_sim)
        diff_estimate = 0
        #self.observable_sim = np.array([])
        #for state in observables:
       # 
        #    for bunch1 in state:
        #        for bunch2 in state:
        #            diff_estimate += abs(bunch1 - bunch2)
        #            
        #self.observable_sim = 
        #self.observable_sim_extended = np.polynomial.polynomial.Polynomial.fit(self.setting_sim, self.observable_sim, 2)
        self.reward_width = 0.25
        # Best rewards for DDPG/NAF
        
        if REWARD_FUNC == 'gausshej':
            self.reward_sim = np.exp(-(self.observable_sim/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
        elif REWARD_FUNC == 'parabola':
            self.reward_sim = 1-(self.observable_sim/(3*self.reward_width))**2+REWARD_OFFSET
        elif REWARD_FUNC == 'linear':
            self.reward_sim = np.abs(self.observable_sim) + REWARD_OFFSET
        #elif REWARD_FUNC == 'simple':
        #    self.reward_sim = -self.observable_sim_extended(self.setting_sim) + REWARD_OFFSET
    
        ### Status of the iterations
        # Steps, i.e. number of cycles
        self.counter = 0
        self.curr_step = -1  ## Not used ?
        self.max_steps = max_steps
        
        # Episodes, i.e. number of MDs, number of LHC fills...
        self.curr_episode = -1
        self.action_episode_memory = []
        self.state_memory = []
        self.phase_volt_set_memory = []
        self.reward_memory = []
        self.is_finalized = False
        
        ### Set the seed
        self.seed(seed)
        
        ### Reset
        #self.reset()
        
        
    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """
        success = False    
        self.curr_step += 1
        #print(f" state before action {self.state}, action {action}")
        self._take_action(action)
        #print(f" state After action {self.state}")
        reward = self._get_reward()
        state = self.state
        if abs(self.diff_estimate) < BUNCH_LENGTH_CRITERIA:
            self.is_finalized = True
            success = True
            #reward = reward + 1 # Reward early succesful optimization?
        if self.counter >= self.max_steps:
            self.is_finalized = True

            ### Don't allow it to go too far from the data
        #if (self.phase_set<self.min_setting-20) or (self.phase_set>self.max_setting+20):
        #    self.is_finalized = True
        #reward = reward + state[1] # Add bonus if observable shrinks, minus otherwise
        self.reward_memory[self.curr_episode].append(reward)
        info = {'success': success, 'steps': self.counter, 'phase_corr': self.phase_correction, 'initial_phase': self.initial_offset}
        
        return state, reward, self.is_finalized, info
    
    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance. 
        Converted back to phase setting in degrees using self.max_step_size.
        """
        
        #action = action[0]
        self.action = action
        converted_action = np.array([0.0,0.0,0.0], dtype=object)
        converted_action[0] = action[0]*self.max_step_size
        converted_action[1] = action[1]*self.max_step_size
        converted_action[2] = action[2]*self.max_step_volt
        self.phase_correction += converted_action

        # Phase offset as action, Best action for DDPG
        if ACTION_TYPE == 'relative':
            self.phase_volt_set += converted_action
        
        # Absolute phase as action, Best action for NAF
        elif ACTION_TYPE == 'absolute':
            self.phase_set = converted_action
    
        self.state = self._get_state()
        
        self.action_episode_memory[self.curr_episode].append(action)
        self.state_memory[self.curr_episode].append(self.state)
        self.phase_volt_set_memory[self.curr_episode].append(self.phase_volt_set)
        
        self.counter += 1
    
    def _get_state(self):
        '''
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        '''

        if (self.phase_volt_set[0]<self.min_setting):
            state = np.array([0.93, 1.0, 0.9])
        elif (self.phase_volt_set[0]>self.max_setting):
            state = np.array([0.9, 1.0, 0.93])
        elif (self.phase_volt_set[1]<self.min_setting): #TESTING
            state = np.array([1.,0.9,0.8])
        elif (self.phase_volt_set[1]>self.max_setting):
            state = np.array([0.8,0.9,1.0])
        elif (self.phase_volt_set[2] > 1.1):
            state = np.array([0.92, 0.9, 0.97])
        elif (self.phase_volt_set[2] < 0.9):
            state = np.array([0.97, 0.9, 0.92])
        else:
            # Interpolating the state/observable from the simulated data
            state = interpolation(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
        
        return state
    
    def _get_reward(self):
        '''
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        '''
        
        observable = self.state
        # Best rewards for DDPG/NAF
        if REWARD_FUNC == 'observable':
            reward = -self.observable_sim_extended(self.phase_set)# + 0.25
        if REWARD_FUNC == 'gauss':
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 10
                
        elif REWARD_FUNC == 'parabola':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = 1-(observable/(3*self.reward_width))**2 - 1 + REWARD_OFFSET
            else:
                reward = 1-(observable/(3*self.reward_width))**2 + REWARD_OFFSET
        
        elif REWARD_FUNC == 'linear':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = -1 + REWARD_OFFSET
            else:
                reward = -np.abs(np.linspace(-45,45,91))/45 + REWARD_OFFSET
        elif REWARD_FUNC == 'simple':
            reward = -1
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.1:
                reward += 0.5
        return reward
    
    def reset(self):
        """
        Reset to a random state to start over the training
        """
        
        # Resetting to start a new episode
        self.curr_episode += 1
        self.counter = 0
        self.is_finalized = False

        # Initializing list to save steps for that episode
        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_volt_set_memory.append([])
        self.reward_memory.append([])
        
        # Getting initial state
        self.phase_set = np.array([0.0,0.0,0.0])
        self.phase_volt_set[0] = random.uniform(self.min_setting,
                                            self.max_setting)
        self.phase_volt_set[1] = random.uniform(self.min_setting,
                                            self.max_setting)
        self.phase_volt_set[2] = random.uniform(0.9,1.1)

        self.initial_offset = np.copy(self.phase_volt_set)
        self.phase_correction = 0
                                        
        #self.prev_state = self.phase_set + 5
        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_volt_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)
        
#        if self.curr_episode == 2:
#            plt.figure('Observable')
#            plt.savefig('observable_init.png')
#            plt.figure('Reward')
#            plt.savefig('reward_init.png')
            
        

        return state

    def set_state(self, phase_volt):

        """
        Reset environment with a specified phase setting. Used to evaluate performance when starting at specified phase offsets.
        """
        self.phase_volt_set = phase_volt
        self.initial_offset = np.copy(self.phase_volt_set)

        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])

        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)

        return state

    def seed(self, seed=None):
        """
        Set the random seed
        """
        
        random.seed(seed)
        np.random.seed
        
    def render(self, mode='human'):
        
        """
        Not implemented, code is from twosplit.
        """

        plt.figure('Observable')
        plt.clf()
        plt.plot(self.setting_sim, self.bunch_length_1, 'b')
        plt.plot(self.setting_sim, self.bunch_length_2, 'r')
        bunch_lengths_1 = np.zeros(len(self.state_memory[self.curr_episode]))
        for i, state in enumerate(self.state_memory[self.curr_episode]):
            bunch_lengths_1[i] = state[0]
        plt.plot(self.phase_set_memory[self.curr_episode], bunch_lengths_1, 'ok')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], bunch_lengths_1[-1], 'or')
        bunch_lengths_2 = np.zeros(len(self.state_memory[self.curr_episode]))
        for i, state in enumerate(self.state_memory[self.curr_episode]):
            bunch_lengths_2[i] = state[1]
        plt.plot(self.phase_set_memory[self.curr_episode], bunch_lengths_2, 'og')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], bunch_lengths_2[-1], 'ob')
        fig = plt.gcf()
        axes = plt.gca()
        
        #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
        plt.pause(0.1)
        
        plt.figure('Reward')
        plt.clf()
        #plt.plot(self.setting_sim, -self.observable_sim, 'b')
        
        #plt.plot(self.setting_sim, self.reward_sim, 'b')
        plt.plot(self.phase_set_memory[self.curr_episode], self.reward_memory[self.curr_episode], 'ok')
        plt.plot(self.phase_set_memory[self.curr_episode][-1], self.reward_memory[self.curr_episode][-1], 'or')
        fig = plt.gcf()
        axes = plt.gca()
      #  plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Reward')
        plt.pause(0.1)
        
        
        
        







