#%%
import gym
import numpy as np
from numpy.core.fromnumeric import mean, shape
from numpy.core.function_base import linspace
from quadsplit_class_bunch_lengths import QuadSplitContinuousBL
from stable_baselines3 import SAC
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.callbacks import EvalCallback
from scipy import interpolate


model = SAC.load("./logs/quad/report_models/sac-simple")
env = QuadSplitContinuousBL()

obs = env.reset()
performance = [0,0]
steps_per_episode = []
predicted_phase_error_when_done = []
for _ in range(500):
    #print(obs)
    action, _states = model.predict(obs)
    #print(action*10)
    obs, rewards, done, info = env.step(action)
    #print(obs)
    #env.render() # Use if you want to observe the agent
    if done:
        print(f"Took {info['steps']} steps before terminating test")
        print(f"Info {info}")
        steps_per_episode.append(info['steps'])
        if info['success'] == True:
            performance[0] += 1
        else:
            performance[1] += 1
        
        predicted_phase_error_when_done.append(info['initial_phase'] + info['phase_corr'])
        obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")
print(f"Mean episode length: {np.sum(steps_per_episode)/len(steps_per_episode)}")
print(f"Max episode length: {np.max(steps_per_episode)}, Min episode length: {np.min(steps_per_episode)}")
p42_err = []
p84_err = []
for entry in predicted_phase_error_when_done:
    p42_err.append(entry[0])
    p84_err.append(entry[1])
plt.plot(p42_err, p84_err, '.')
plt.title('SAC-Gauss-corr: Scatter plot of correction errors')
plt.xlabel('p42 prediction error')
plt.ylabel('p84 prediction error')
plt.xlim(-5,5)
plt.ylim(-5,5)
plt.show()

# %% Evaluation

# Loading simulation data
loaded_results = np.load('..\lookup_table_bunch_lengths_quad_corrected.npy')
phase = np.linspace(-45,45,91) #loaded_results['dpc20'] + OFFSET
bunch_lengths = loaded_results # Bunch lengths are normalized between 0 and 1.

rows, cols, lengths = np.shape(bunch_lengths)
matrix_of_phase_1_errors = np.zeros((np.shape(bunch_lengths[:,:,0])))
matrix_of_phase_2_errors = np.zeros((np.shape(bunch_lengths[:,:,0])))

print(np.shape(matrix_of_phase_1_errors))

for row in range(rows):
    for col in range(cols):
        mean_phase_error = 0
        for i in range(10):
            first_guess = True
            env.reset()
            env.set_state(np.array([np.float32(row-45),np.float32(col-45)]))
            done = False
            while not done:
                if first_guess:
                    action, _states = model.predict(bunch_lengths[row,col])
                    first_guess = False
                else:
                    action, _states = model.predict(obs)
                obs, rewards, done, info = env.step(action)
            mean_phase_error += info['initial_phase'] + info['phase_corr']
            #print(mean_phase_error)
        mean_phase_error = mean_phase_error/10
        matrix_of_phase_1_errors[row,col] = mean_phase_error[0]
        matrix_of_phase_2_errors[row,col] = mean_phase_error[1]

plt.figure()
im1 = plt.imshow(matrix_of_phase_1_errors, extent=[-45,45, 45,-45])
plt.title('SAC-Simple: Matrix of phase 42 correction errors')
plt.xlabel('p84 initial setting')
plt.ylabel('p42 initial setting')
plt.colorbar(im1, orientation='horizontal')

plt.show()

plt.figure()
im2 = plt.imshow(matrix_of_phase_2_errors, extent=[-45,45, 45,-45])
plt.title('SAC-Simple: Matrix of phase 84 correction errors')
plt.xlabel('p84 initial setting')
plt.ylabel('p42 initial setting')
plt.colorbar(im2, orientation='horizontal')
plt.show()