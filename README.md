<a href="https://summerstudent.web.cern.ch/home" target="_blank"><img height="45px" src="https://img.shields.io/badge/CERN%20Summer%20Student-2021-5c5c5c?style=flat&labelColor=0033A0&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAAPFBMVEVHcEz///////////////////////////////////////////////////////////////////////////+PybD1AAAAE3RSTlMAJkI02ugG+vLHDoyzoBlRXm18cLn1dwAAB1BJREFUeNqsVoea4ywMDL2b9v7v+gcEkU0gf7u5211/FI3KSPbrN+RFGI0x5Y4UV9A72A0EcV0/CCxLzhsutDpD3yFu4HccGagzuv4h7CmuDAx/CHsKXm/oSVH1gZkO8YC+QyE2tU6mTpOae5cjZYzG7DzH6HgmVwO5gz1AEV8cJHwYTKGXfCghezE3A3n9Z0Q+fXXMbsIkxYATisf/SCGznuk4itumQaOL/W3scN/BdVGA4kxTO8KPYzR4n3aXAyTCs7P9i7SKThZ/Lkyz5eWJQ2S7j57E0gdARwV4e8qV35JIB9Wg2ztNWKp+Qa3ymLAt1vC1XLoNw3Y3YhD1AO23GrmaSt26mnTnICc5IdQbFXCWImlelWWR8RMH9R+TSphQUqQ0phLuA8Aku9prm2nx1neX2HfYbprSxkVi768aWkBkO0VS9V5c2jV3M+k7jGlGODoJVn8BPD2klJq9JzHhFeq0IIlhoZC9UN1daHbxWpCvzqnmWk0UPaI49tw1I10HQGPnD4NMtODiYcgY+jqD6h6pxgGAbhv7FUiQKwfU2/2cYxKSkPjCsjY8aFqwbbpFOhNgFlQkHliuU8OXHshOODhkznBdwa8rQF0skCwND6ya7lp2aZxzVQSb89XJj1zLeup7nEa+cJznLRiEhlZpbXjMVv6+TeI/e40nNZQEeuZk0/DWY99YWsIbLjFLc04NOUfWHnPOici3yVBmcdveHLiagQiGTKMaS5j8qbb4GbfCd9HAs6kDwlnLh4TAW34NFUMBshoNtzY8VSAE/IpYoDk+u+42sEQ1DOU6iyr9eCxLw+eu8+lGVdx0LoUkpv/uC4KZj1AjRAJVBrrZNS+3NLyDE5C2auJlWVCVi97IOReXQztBY089NaMdMF1QlKElB6EsnxHSj8gcvrVkcrEZ849h5yYJsHTpG0xSwApr6p/tLQ2QWjNFjsuGxpQYkNAMIjKzOpJ1EhziFkMJBhsep8yIGWsFnEq1y/3emLM42x2SFCjXbaLrR8OD8YBKRhIAkAwY0iJUoAzfSCRopyFh+jvSQuIGidmQcCTxpK+LPEQ9vUoQ2m0AgMZWEvgryKMm3LngM6QZqg0kJApYwEiAEF/lz4a/Rk3AdTcoGOvLqC6tYdvyfj1pjARIsKAjX4IshfdyVi9J+A4SGkiktD1dvOiuvkECLDMSCFVT7G7kxEIZO8PUIdFsZjmC9+aN7pcDJ/ibio75gBLuyPi6W9/wYYY2Eq1U3UAQEK/Qs6RFTUtyNuityvWvTq1EOVYQCJaAIHKz//+vLxun2mPUMK/rjOXaMve0OXf4jpPW+QPsMyjcqHCpYAjwK56Kd7cRwQbU/aQ60mHmpPyH5Aa3GOfctxUFt4GMbxM9V7mDq+Gizno0ndTq2kPv35aoi9pQ1FRU0d8Y+f2rFGXRtRQS/pAZ/ZqLrJ7Ised4h23AiHvOl9bVChgw15jd18VdchF1E/Ov914NEyAvQtrtoRy3TYXT9qlsFKjXpe1vrxDcAM0Ync2XA4BxzPr75LZfSexVcDxUGaHbw1YwAkzDrIG43quM4CA0pdIPEcKVHRiTH6qMwCNkm4AfJnIPi0EsKKgyY2jovHXLBOI1+T6blnKsMgNA0AQU334eSdmCQgbTiyC81rgbeqV03Kzfb1YMyDHDVQaFGHNUwgBHduM7LDaYRPcMoLmjUtQ3f2fuErwEWOpn1F51gbGQ6BWpyFChzQQ7zYNBXOc9XBCkaXvh+lBIiSZlrBvPgJKAGzHZ6vl4Ol7uN6b41QjcX67PpOym635AVYaDnEaiz+L+TpVCoeLzxejOIZnvgzhgZL8/MTCFBTfdaUbJPgsxfp3iBwiPluoQqBWXBN+qUtpKUCbRBpbgiicoPvM1m+LrQfaSZauBGNf0emGYKg1N0OCYZ7lKc7doT2nBq/pQ1ZTXH+RJ10AMG1y7L8muvzc4M0HaBJbZ+B+YGdTE0ZlRtGEz8N1RCzGWeH6k+zimOjOOyX/eSTBIwr/+QWsmnfuJ470irR4Rha3sDLeY2EtEoj/orq8VPJ0j3PaNxYQfpBB6KzpbG9wtBz7uvJc93tA2FhdxfDDH/CBRRwyrb0HsV/5ZyDRcfOCATJFJ3ngP4lnfaM/Ol3XvhO4q6Ou0QH3+c9Zp3GvaU0zFst5/mLA6UIbOjYaI1wKujXOz4sam0/T2KzMZfSozSCivDt3xjUUpba9GLOeEAceaVYt7rQz5kAjSr3ao7NANQ2+th+QPq+sSFdulpZgCaM6Aw+zuWFQnOU16pDBJWdZi/4/E38oIi0n1YprM01HEsfTSk98+jxvjY2gKacfURjFyRJOyds3b/z087ciBp6OAo4/YAOkoxJrQpEaAdJRzfEY5kI4ijoAmNQRsjAJYGQfqbJVwoIGMQ5qOtsk5xOnY0EAEsFFUI6uYA+noRtOxzGggImAikXBIocfTURlsIEJA9hi4ExuIFJZ2nSESBw4hqnNY9N6hW2tKToAPkMdE+QcS1+0WlPAXfQAAAABJRU5ErkJggg==" /></a>


# Background

This project was written as a part of the Summer student program 2021 at CERN by Joel Wulff. Supervisors were Alexandre Lasheen and Mihaly Vadai. The full report of the project can be read here: TO BE ADDED!!!

# Introduction

In order to create the high-energy particle beams used in the CERN accelerator complex andthe LHC a great amount of care is needed. One step in the chain of accelerators is the ProtonSynchrotron (PS), in which the proton beam is accelerated from 1.4 GeV to 25 GeV. But during this accelerating process, it is not only the energy that is important, but alsothe longitudinal beam structure (i.e. number of bunches, bunch length/intensity, longitudinal emittance...). This structure is defined bu a sequence of RF manipulations in the PS to bringthe bunch distance down to 25 ns and a target longitudinal emittance ofl= 0.35eVs. In orderto do this efficiently, the RF settings (voltage, phase, timings) for each manipulation must be optimized to produce bunches with near identical intensity and emittance. An example of an RF manipulation scheme can be seen in Fig. 1 below.

![Figure 1](bcms.PNG)

n this summer project, the focus has been on the RF-manipulations for the quadruplesplitting, pointed out in the image. Presently, the longitudinal beam quality is ensured byregular checks before filling the LHC. However, this does not always result in the best achievableperformance. In the past, different methods including classical optimization, tomography andsome machine learning have been investigated in simulations. In this project, I am continuingthe work of previous summer student Sarah Johnston and my supervisor Alexandre Lasheenby investigating the use of machine learning further, specifically the viability of Reinforcement Learning models to optimize different forms of bunch splittings.

# Packages used

In this project, the following packages have been used:

- python 3.9.5 (An Anaconda distribution running on a windows 10 computer. Code should generally be os independent, but has not been tested)
- BLonD: Beam Longitudinal Dynamics. Used for simulating beam. https://github.com/blond-admin/BLonD
- stable_baselines3: Documentation at https://stable-baselines3.readthedocs.io/en/master/guide/install.html. Used for implementations of SAC and TD3
- Numpy
- Scipy
- OpenAI Gym: https://gym.openai.com/
- matplotlib

# Gitlab structure

Some information about where to find the most relevant files in the repository is described here. For code implementing the reinforcement learning parts of the code, go to the RL folder. Here you will find,

- Gym environments for the different cases
    - twosplit_class_bunch_lengths.py
    - quadsplit_class_bunch_lengths.py
    - trisplit_class_bunch_lengths.py (only bunch lengths as observable)
    - trisplit_class_bl_bi.py (bunch length and bunch intensity as observable)
- Scripts to train new models using the above classes
    - train_twosplit_bunch_lengths.py
    - train_quadsplit_bunch_lengths.
    - train_trisplit_bunch_lengths.py
    - train_trispit.py (bl_bi)
- Scripts to evaluate train models in simulated environments
    - run_model (twosplit)
    - run_model_quad
    - run_model_tri
In the base directory a folder called report_models contains all trained models presented in the report. An important parameter when loading and evaluating these models is the BUNCH_LENGTH_CRITERIA/DIFF_ESTIMATE it was trained with. The following were used:
- Twosplit
    - BUNCH_LENGTH_CRITERIA = 0.002
- Quadsplit
    - DIFF_ESTIMATE = 0.04
- Trisplit
    - Bunch lengths only: DIFF_ESTIMATE = 0.08
    - Bunch lengths and bunch intensity: DIFF_ESTIMATE = 0.2

The BUNCH_LENGTH_CRITERIA/DIFF_ESTIMATE is defined in the environment class corresponding to the splitting.
# Environments

## Custom environments

The custom environments in this project were made using OpenAI's Gym interface. This was done in order to ease the use of different pre-written RL algorithms and ensure compatability with the stable_baselines3 code.

## Twosplit

The twosplit environment is contained in the **twosplit_class_bunch_lengths.** file.  Bydefault it loads simulated data from a file and creates an interpolating function between data-points to allow for continuous sampling of the observables. In the__init__of the environmentthe maximum number of steps per episode and the maximum step size are assigned, and theyare important for the training and the final performance of the final function approximator.

The **Action space** of the environment is one-dimensional, and restricted to floats between -1 and 1 (this normalization helps with the optimization of the model). The actions output by the model will get scaled with the \verb|max_step_size| parameter when getting the next state. All actions are taken relative to the current position. The physical interpretation of this is that we change the phase of the RF corresponding to the splitting by a certain amount from the current setting. This is defined by the code below:

                ...
                self.action_space = gym.spaces.Box(
                            -1,
                            1,
                            shape=(1,),
                            dtype=np.float32)
                ...

The **Observation space** of the environment is two-dimensional, and each state is defined by two values: the lengths of the two bunches. The bunch lengths are normalized, so that all lengths are in the range [0,1]. See code snippet below.

                ...
                self.observation_space = gym.spaces.Box(
                            np.array([0, 0]),
                            np.array([1, 1]),
                            shape=(2,),
                            dtype=np.float32)
                ...


See the plot below for a plot of how the two bunch lengths change when varying the phase setting.

![](twosplit_obs.png)

For the **Reward function** of the environment, two types of functions were tested. The first is called 'simple', and consists of giving a fixed reward based on the absolute difference of the two bunch lengths. Given a small enough difference, the model will receive a large reward and exit the current episode, and otherwise it will get a negative reward. The other is 'gauss', which gives a reward based on a Gaussian distribution centered around 0 and offset by -1. This means that the larger the distance between the two bunch lengths, the larger negative reward will be observed after taking an action.

In general, the twosplit training was limited to a maximum number of 10000 timesteps.

## Quadsplit

The quadsplit environment is contained in the \verb|quadsplit_class_bunch_lengths.py| file. It loads simulated data from a file containing four bunch lengths for each simulated phase setting, and creates an interpolating function that linearly interpolates the data along each bunch to allow for continous datapoints. In general, the structure of the script is similar to that of the twosplit environment.

The **Action space** of the environment is two-dimensional, and restricted to floats between -1 and 1. The two parameters it can change are the phase offsets of the two active RF-systems responsible for the quad splitting.


The **Observation space** of the environment is four-dimensional, and each state is defined by four values: the lengths of the four bunches. The bunch lengths are normalized, so that all lengths are in the range [0,1].

The **Reward function** is of the same two types as in the twosplit environment. However, as their is now four bunch lengths to consider, it is not as simple to compute a metric to determine their similarity. For each bunch length, the absolute difference between its length and all the other bunches is summed up and added to a difference estimate. Then, the same is done for each of the other bunches, adding this to the difference estimate. This method could be used for any number of bunch lengths, which will be useful if for example attempting to optimize a triple splitting instead. Then, however, you may want to add some normalization factor to avoid having to check what value constitutes a "good" splitting manually.

In general, the quadsplit training was limited to a maximum of 50000 timesteps.

## Trisplit

An environment for the more complicated triple splitting performed by the RF cavities with $h=14$ and $h=21$ harmonics was also created, but as it was near the end of the project it has not gone through the same amount of testing as the previous environments. It is included to aid the continued work towards a more succesful model.

The **Action space** here is three-dimensional, and restricted to floats between -1 and 1. The first two dimensions correspond to real phase offsets of [-15,15] degrees and the last dimension corresponds to a voltage factor in the range [0.9,1.1] in the $h=14$ harmonics voltage program.

The **Observation space** of the environment is now six-dimensional, where each state is defined by six values: the lengths of the three bunches followed by the intensities of the three bunches. Both bunch lengths and bunch intensities are normalized by division with the mean of the three bunches length/intensity in the current observation, centering them around 1. The observation space is limited to the range [0,2]. **NOTE: modified BLonD source code to gather bunch intensities along with bunch lengths**.

The **Reward functions** of the environment are identical to those in the quadsplit, but with a changed difference estimate signifying an acceptable splitting.

# Future work

## Improving description of state

Describing the state of the environment using only the bunch lengths allowed for the agent to take full observations of the state it was in. All of this information would also be available when the model eventually attempts to run in the real PS. However, I believe there may be times when a state is not uniquely defined by only the bunch lengths, which would lead to a breaking of one fundamental concept of RL: That all information about the world is contained in the state.

By including the true phase offset in the state, we could more accurately decide what degree of accuracy the RL agent will learn by deciding the reward function or stopping condition of the model using the true phase offset. A problem that then arises is that when using the model in the actual machine, we will not have access to the true phase offset, so a different exit condition would need to be designed for training vs. prediction. The model would also of course have to be trained using only partial observations of the environment, namely the data it will have access to in the real machine (the bunch lengths). The learned model may not perform much better than the ones presented in this paper, but they would be easier to train to a set accuracy
