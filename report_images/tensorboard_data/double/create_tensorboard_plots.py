# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 12:57:48 2021

@author: Joel Wulff
"""
#%% Actor losses
import json
import matplotlib.pyplot as plt

f1 = open('./actor_loss/SAC_Gauss-corr.json',)
data1 = json.load(f1)

f2 = open('./actor_loss/SAC_Gauss-auto.json',)
data2 = json.load(f2)
f3 = open('./actor_loss/SAC_Gauss.json',)
data3 = json.load(f3)
f4 = open('./actor_loss/SAC_Simple.json',)
data4 = json.load(f4)
f5 = open('./actor_loss/SAC_Simple-corr.json',)
data5 = json.load(f5)

#%%

timesteps1 = []
actor_loss1 = []
for entry in data1:
    timesteps1.append(entry[1])
    actor_loss1.append(entry[2])
    
timesteps2 = []
actor_loss2 = []
for entry in data2:
    timesteps2.append(entry[1])
    actor_loss2.append(entry[2])

timesteps3 = []
actor_loss3 = []
for entry in data3:
    timesteps3.append(entry[1])
    actor_loss3.append(entry[2])

timesteps4 = []
actor_loss4 = []
for entry in data4:
    timesteps4.append(entry[1])
    actor_loss4.append(entry[2])

timesteps5 = []
actor_loss5 = []
for entry in data5:
    timesteps5.append(entry[1])
    actor_loss5.append(entry[2])

plt.figure()
ax = plt.gca()
plt.plot(timesteps1, actor_loss1, label='SAC-Gauss-corr')
plt.plot(timesteps2, actor_loss2, label='SAC-Gauss-auto')
plt.plot(timesteps3, actor_loss3, label='SAC-Gauss')
leg = plt.legend(bbox_to_anchor=(0.65, 0.15, 0.3, 0.2), loc='center right')
plt.title('Actor loss during training')
plt.ylabel('Actor loss')
plt.xlabel('Timesteps')

plt.figure()
plt.plot(timesteps4, actor_loss4, label='SAC-Simple')
plt.plot(timesteps5, actor_loss5, label='SAC-Simple-corr')
leg = plt.legend(bbox_to_anchor=(0.65, 0.3, 0.3, 0.2), loc='center right')
plt.title('Actor loss during training')
plt.ylabel('Actor loss')
plt.xlabel('Timesteps')

#%% Critic losses
import json
import matplotlib.pyplot as plt

f1 = open('./critic_loss/SAC_Gauss-corr.json',)
data1 = json.load(f1)

f2 = open('./critic_loss/SAC_Gauss-auto.json',)
data2 = json.load(f2)
f3 = open('./critic_loss/SAC_Gauss.json',)
data3 = json.load(f3)
f4 = open('./critic_loss/SAC_Simple.json',)
data4 = json.load(f4)
f5 = open('./critic_loss/SAC_Simple-corr.json',)
data5 = json.load(f5)

#%%

timesteps1 = []
loss1 = []
for entry in data1:
    timesteps1.append(entry[1])
    loss1.append(entry[2])
    
timesteps2 = []
loss2 = []
for entry in data2:
    timesteps2.append(entry[1])
    loss2.append(entry[2])

timesteps3 = []
loss3 = []
for entry in data3:
    timesteps3.append(entry[1])
    loss3.append(entry[2])

timesteps4 = []
loss4 = []
for entry in data4:
    timesteps4.append(entry[1])
    loss4.append(entry[2])

timesteps5 = []
loss5 = []
for entry in data5:
    timesteps5.append(entry[1])
    loss5.append(entry[2])

plt.figure()
ax = plt.gca()
plt.plot(timesteps1, loss1, label='SAC-Gauss-corr')
plt.plot(timesteps2, loss2, label='SAC-Gauss-auto')
plt.plot(timesteps3, loss3, label='SAC-Gauss')
leg = plt.legend(loc='upper right')
plt.title('Critic loss during training')
plt.ylabel('Critic loss')
plt.xlabel('Timesteps')

plt.figure()
plt.plot(timesteps4, loss4, label='SAC-Simple')
plt.plot(timesteps5, loss5, label='SAC-Simple-corr')
leg = plt.legend(bbox_to_anchor=(0.65, 0.3, 0.3, 0.2), loc='center right')
plt.title('Critic loss during training')
plt.ylabel('Critic loss')
plt.xlabel('Timesteps')

#%% Episode length

f1 = open('./ep_len_mean/SAC_Gauss-corr.json',)
data1 = json.load(f1)

f2 = open('./ep_len_mean/SAC_Gauss-auto.json',)
data2 = json.load(f2)
f3 = open('./ep_len_mean/SAC_Gauss.json',)
data3 = json.load(f3)
f4 = open('./ep_len_mean/SAC_Simple.json',)
data4 = json.load(f4)
f5 = open('./ep_len_mean/SAC_Simple-corr.json',)
data5 = json.load(f5)

#%%

timesteps1 = []
loss1 = []
for entry in data1:
    timesteps1.append(entry[1])
    loss1.append(entry[2])
    
timesteps2 = []
loss2 = []
for entry in data2:
    timesteps2.append(entry[1])
    loss2.append(entry[2])

timesteps3 = []
loss3 = []
for entry in data3:
    timesteps3.append(entry[1])
    loss3.append(entry[2])

timesteps4 = []
loss4 = []
for entry in data4:
    timesteps4.append(entry[1])
    loss4.append(entry[2])

timesteps5 = []
loss5 = []
for entry in data5:
    timesteps5.append(entry[1])
    loss5.append(entry[2])

plt.figure()
ax = plt.gca()
plt.plot(timesteps1, loss1, label='SAC-Gauss-corr')
plt.plot(timesteps2, loss2, label='SAC-Gauss-auto')
plt.plot(timesteps3, loss3, label='SAC-Gauss')
plt.plot(timesteps4, loss4, label='SAC-Simple')
plt.plot(timesteps5, loss5, label='SAC-Simple-corr')
leg = plt.legend(loc='upper right')
plt.title('Mean episode length')
plt.ylabel('Mean episode length')
plt.xlabel('Timesteps')

#%% Episode length

f1 = open('./ep_rew_mean/SAC_Gauss-corr.json',)
data1 = json.load(f1)

f2 = open('./ep_rew_mean/SAC_Gauss-auto.json',)
data2 = json.load(f2)
f3 = open('./ep_rew_mean/SAC_Gauss.json',)
data3 = json.load(f3)
f4 = open('./ep_rew_mean/SAC_Simple.json',)
data4 = json.load(f4)
f5 = open('./ep_rew_mean/SAC_Simple-corr.json',)
data5 = json.load(f5)

#%%

timesteps1 = []
loss1 = []
for entry in data1:
    timesteps1.append(entry[1])
    loss1.append(entry[2])
    
timesteps2 = []
loss2 = []
for entry in data2:
    timesteps2.append(entry[1])
    loss2.append(entry[2])

timesteps3 = []
loss3 = []
for entry in data3:
    timesteps3.append(entry[1])
    loss3.append(entry[2])

timesteps4 = []
loss4 = []
for entry in data4:
    timesteps4.append(entry[1])
    loss4.append(entry[2])

timesteps5 = []
loss5 = []
for entry in data5:
    timesteps5.append(entry[1])
    loss5.append(entry[2])

plt.figure()
plt.plot(timesteps1, loss1, label='SAC-Gauss-corr')
plt.plot(timesteps2, loss2, label='SAC-Gauss-auto')
plt.plot(timesteps3, loss3, label='SAC-Gauss')
leg = plt.legend(loc='center right')
plt.title('Mean episode reward - Gaussian reward function')
plt.ylabel('Mean episode reward')
plt.xlabel('Timesteps')

plt.figure()
plt.plot(timesteps4, loss4, label='SAC-Simple')
plt.plot(timesteps5, loss5, label='SAC-Simple-corr')
leg = plt.legend(loc='center right')
plt.title('Mean episode reward - Simple reward function')
plt.ylabel('Mean episode reward')
plt.xlabel('Timesteps')

