# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:36:32 2021

@author: Joel Wulff
"""

# General imports
import scipy as scp
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib widget
import time
import datetime
from scipy import fftpack
import blond

# BLonD imports
from blond.input_parameters.ring import Ring, RingOptions
from blond.input_parameters.rf_parameters import RFStation
from blond.beam.beam import Proton, Beam
from blond.beam.profile import Profile, CutOptions
from blond.trackers.tracker import RingAndRFTracker, FullRingAndRF
# from blond.impedances.impedance_sources import Resonators, InputTable
# from blond.impedances.impedance import InducedVoltageFreq, InductiveImpedance, TotalInducedVoltage
from blond.beam.distributions_multibunch import match_beam_from_distribution

class Simulation():
    
    def __init__(self, initial_phase_errors = None, n_bunches=1, 
                 n_slices_per_bunch=2**10, n_rf_systems=3, n_macroparticles_per_bunch = int(1e4), split_type = 2):
        """
        Constructor
        """

        self.initial_phase_errors = initial_phase_errors
        self.optimizer = 'powell'
        # Simulation parameters
        self.plot_turn = 5000
        self.turn = 0
        time_sim_start = 2710e-3
        time_sim_end = 2843e-3
        
        # General parameters
        circumference = 2.*np.pi*100.                    # Machine circumference [m]
        self.gamma_transition = 6.1                           # Transition gamma
        momentum_compaction = 1./self.gamma_transition**2  # Momentum compaction array
        particle_type = Proton()
        ramp_interpolation = 'linear'
        loaded_momentum = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/ramp.npz')
        time_momentum = loaded_momentum['time']*1e-3        # Time [s]
        momentum = loaded_momentum['momentum']*1e9          # Momentum [eV/c]
        
        #plt.plot(time_momentum, momentum)

        #RF parameters
        self.split_type = split_type                                          # Currently using double splittings
        self.n_rf_systems = n_rf_systems                                # Number of rf systems 
        self.harmonic_numbers = [21, 42, 84]                          # Harmonic numbers
        self.loaded_h21 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C10_h-21.npz')
        self.time_h21 = self.loaded_h21['time']*1e-3                          # Time [s]
        self.voltage_h21 = self.loaded_h21['amplitude']*1e3                   # Voltage [V]
        self.loaded_h42 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C20.npz')
        self.time_h42 = self.loaded_h42['time']*1e-3                          # Time [s]
        self.voltage_h42 = self.loaded_h42['amplitude']*1e3                   # Voltage [V]
        self.loaded_h84 = np.load('./input_files/LHC25#48b_BCMS_PS_TFB_2018/C40.npz')
        self.time_h84 = self.loaded_h84['time']*1e-3                          # Time [s]
        self.voltage_h84 = self.loaded_h84['amplitude']*1e3                   # Voltage [V]
        #plt.plot(time_h21, voltage_h21)
        #plt.plot(time_h42, voltage_h42)
        #plt.plot(time_h84, voltage_h84)
        #plt.xlim((time_sim_start,time_sim_end))
        #print(voltage_h42[time_h42<=time_sim_end][-2])
        #plt.ylim((0, 1.1*voltage_h84[time_h84<=time_sim_end][-1]))
        
        # Phase offsets, the possible errors
        #if self.initial_phase_errors.all == None:
        #    phase_err_rad = [0]*n_rf_systems # Default is no offset
        #else:
        phase_err_rad = [phase / 180*np.pi for phase in self.initial_phase_errors]
        #dpc20 = 0
        #dpc40 = 0
        phi_offsets =  [0, phase_err_rad[0], 2*phase_err_rad[0]+phase_err_rad[1]]#dpc20/180*np.pi, dpc40/180*np.pi]     # For 2x2 splitting
        
        # Beam parameters
        self.n_bunches = n_bunches
        
        initial_bunch_length_full = 52e-9
        initial_exponent = 1.
        
        self.n_macroparticles_per_bunch = n_macroparticles_per_bunch
        intensity_per_bunch = 1.3e11
        
        intensity = intensity_per_bunch*n_bunches*4
        n_macroparticles = int(n_macroparticles_per_bunch*n_bunches*4)
        
        # Profile parameters
        self.n_slices_per_bunch = n_slices_per_bunch
        self.bunch_spacing_buckets = 1
        n_slices = int(self.n_slices_per_bunch*(self.bunch_spacing_buckets*(self.harmonic_numbers[0]-1)+1))
        cut_left = 0.
        cut_right = 2*np.pi*(self.bunch_spacing_buckets*(self.harmonic_numbers[0]-1)+1)
        
        # Ring object
        momentum = momentum[(time_momentum>=time_sim_start)*(time_momentum<=time_sim_end)]
        time_momentum = time_momentum[(time_momentum>=time_sim_start)*(time_momentum<=time_sim_end)]
        
        ring_options = RingOptions(interpolation=ramp_interpolation,
                                      t_start=time_sim_start,
                                      t_end=time_sim_end)
        self.ring = Ring(circumference, momentum_compaction,
                    (time_momentum, momentum), particle_type,
                    RingOptions=ring_options)
        
        # RFStation object

        voltage_h21 = self.voltage_h21[(self.time_h21>=time_sim_start)*(self.time_h21<=time_sim_end)]
        time_h21 = self.time_h21[(self.time_h21>=time_sim_start)*(self.time_h21<=time_sim_end)]
        
        voltage_h42 = self.voltage_h42[(self.time_h42>=time_sim_start)*(self.time_h42<=time_sim_end)]
        time_h42 = self.time_h42[(self.time_h42>=time_sim_start)*(self.time_h42<=time_sim_end)]
        
        voltage_h84 = self.voltage_h84[(self.time_h84>=time_sim_start)*(self.time_h84<=time_sim_end)]
        time_h84 = self.time_h84[(self.time_h84>=time_sim_start)*(self.time_h84<=time_sim_end)]
        
        #,(time_h84, voltage_h84)
        self.rf_station = RFStation(self.ring, self.harmonic_numbers,
                               ((time_h21, voltage_h21),
                                (time_h42, voltage_h42),
                                (time_h84, voltage_h84)),
                               phi_offsets,
                               n_rf=self.n_rf_systems)
        # Beam
        self.beam = Beam(self.ring, n_macroparticles, intensity)
        
        
        ### Profile
        cut_options = CutOptions(cut_left, cut_right, n_slices=n_slices,
                                 cuts_unit='rad', RFSectionParameters=self.rf_station)
        self.profile = Profile(self.beam, CutOptions=cut_options)
        # Tracker

        longitudinal_tracker = RingAndRFTracker(self.rf_station, self.beam)
        
        self.full_tracker = FullRingAndRF([longitudinal_tracker])
        self.datamatrix = None
        
        
        ### Beam generation
        distribution_options = {'type': 'binomial', 'exponent': initial_exponent,
                                'bunch_length': initial_bunch_length_full,
                                'bunch_length_fit': 'full', 
                                'density_variable': 'Hamiltonian'}
        
        match_beam_from_distribution(self.beam, self.full_tracker, self.ring,
                                     distribution_options, self.n_bunches,
                                     self.bunch_spacing_buckets,
                                     n_points_potential=int(1e3))
        self.profile.track()
        plt.figure()
        plt.plot(self.profile.bin_centers, self.profile.n_macroparticles)
        plt.xlim((0, self.rf_station.t_rf[0,0]))
        
        plt.figure()
        plt.plot(self.beam.dt, self.beam.dE, '.')
        
    
    def run_sim(self):
        # Move the bunches in the center of the turn
        #self.beam.dt += self.rf_station.t_rf[0,0]
        
        self.datamatrix = np.zeros((int(self.ring.n_turns/100)+1, len(self.profile.n_macroparticles)))
        
        for turn in range(self.ring.n_turns):
        
            if(turn%(self.plot_turn)==0):
                t0 = time.perf_counter()
                
            if (turn%100==0):
                self.datamatrix[int(turn/100), :] = self.profile.n_macroparticles
        
            # Track
            self.full_tracker.track()
            self.profile.track()
            
        
            if(turn%self.plot_turn==0):
                t1 = time.perf_counter()
                print('Turn %d over %d, ETC: %s' %(
                    turn, self.ring.n_turns, datetime.timedelta(seconds=(t1-t0)*(self.ring.n_turns-turn))))
        self.turn = turn # Extract final turn for analysis
        plt.figure()
        plt.plot(self.profile.bin_centers, self.profile.n_macroparticles)
        plt.xlim((0, self.rf_station.t_rf[0,0]))
        plt.figure()
        plt.imshow(self.datamatrix, aspect='auto', origin='lower')
        #plt.xlim((0, self.rf_station.t_rf[0,0]))
        
        
    def process_data(self, opt_param='length'):
        bunches_after_split = self.n_bunches * self.split_type * (self.rf_station.n_rf-1)
        bucket_size_tau = self.rf_station.t_rf[self.rf_station.n_rf-1,self.turn] # At the end, only the second harmonic is active
        #print(bucket_size_tau)
        self.profile.rms_multibunch(bunches_after_split, self.bunch_spacing_buckets, bucket_size_tau, bucket_tolerance=0)
        # Bunch lengths collected in self.profile.bunchLength
        buckets = [0]*84
        # Copy the 4 bunch lengths to create larger signal (clearer spectrum)
        for i in range(0,21):
            a = int(i*4)
            buckets[a:a+4] = self.profile.bunchLength
        print("Bunch lengths: {}".format(self.profile.bunchLength))
        buckets = buckets - np.mean(buckets) #normalize
        f = np.arange(len(buckets)/2+1)
        plt.figure()
        plt.plot(np.arange(len(buckets)), buckets)
        
        transform = np.fft.fft(buckets)
        plt.figure()
        plt.title("FFT, unchanged")
        plt.stem(np.arange(len(buckets)), np.abs(transform))
        plt.figure()
        f = f/84
        plt.title("FFT, x-axis / 84, positive freq (0:42)+1")
        spectral_amp = np.abs(transform)
        plt.stem(f, spectral_amp[:len(f)])
        return spectral_amp[21]**2 + spectral_amp[42]**2

        
    def bunch_length(self):
        bunches_after_split = self.n_bunches * self.split_type * (self.rf_station.n_rf-1)
        bucket_size_tau = self.rf_station.t_rf[self.rf_station.n_rf-1,self.turn] # At the end, only the second harmonic is active
        #print(bucket_size_tau)
        self.profile.rms_multibunch(bunches_after_split, self.bunch_spacing_buckets, bucket_size_tau, bucket_tolerance=0)
        # Bunch lengths collected in self.profile.bunchLength
        return self.profile.bunchLength
    
    def get_datamatrix(self):
        return self.datamatrix, self.profile, self.rf_station