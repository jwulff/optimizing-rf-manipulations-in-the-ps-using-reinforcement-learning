# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:44:50 2021

@author: Joel Wulff
"""

# -*- coding: utf-8 -*-

import numpy as np
import os
import matplotlib.pyplot as plt


#loaded_values = []
first_file = True

for file in os.listdir('datamatrix_files\quad'):
    #print(file)
    if file.endswith("blengths.npy"):
        # print path name of selected files
        file_path = os.path.join('datamatrix_files\quad', file)
        #print(file_path)
        if first_file:
            loaded = np.load(file_path, allow_pickle=True)
            loaded_values = np.array([loaded[0], loaded[1], loaded[2]], dtype=object)# phase 42, phase 84, np.array([bunch lengths])
            first_file = False
        else:    
            loaded = np.load(file_path, allow_pickle=True)
            loaded_values = np.vstack((loaded_values, np.array([loaded[0], loaded[1], loaded[2]], dtype=object)))
        
# loaded_values now contain all information needed to construct the matrix
# Format: [[phase_42, phase_84, objective_function value], ... ]

nbr_samples, cols = np.shape(loaded_values)
uniq_p42 = sorted(set(loaded_values[:,0]))
uniq_p84 = sorted(set(loaded_values[:,1]))



matrix = np.zeros((len(uniq_p42),len(uniq_p84), 4))
p42_labels = [0]*len(uniq_p42)
p84_labels = [0]*len(uniq_p84)
col_dict = {}
row_dict = {}
for i, p1 in enumerate(uniq_p42):
    p42_labels[i] = p1
    p1_idx = np.where(loaded_values[:,0]==p1)
    for idx in p1_idx:
        for entry in loaded_values[idx,:]:
            col_dict["{}".format(entry[1])] =  entry[2]
            row_dict = {"{}".format(p1): col_dict}
            #print(col_dict)
        #print(row_dict)
    for j, p2 in enumerate(uniq_p84):
        p84_labels[j] = p2
        matrix[i,j] = row_dict["{}".format(p1)]["{}".format(p2)]
    

matrix = matrix/np.amax(matrix) # normalize
np.save('lookup_table_bunch_lengths_quad', matrix)
# %%
# make plot
fig, ax = plt.subplots()
  
# show image
shw = ax.imshow(matrix[:,:,3], extent=[min(uniq_p84),max(uniq_p84), max(uniq_p42), min(uniq_p42)])
  
# make bar
bar = plt.colorbar(shw)
  
# show plot with labels
plt.xlabel('phi_84')
plt.ylabel('phi_42')
bar.set_label('ColorBar')
plt.show()