# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:31:52 2021

@author: Joel Wulff
"""
from scipy import fftpack
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate as interp

from trisplit_simulation import TrisplitSimulation

def simulate_bunch_lengths(initial_phase_errors, v14_factor):
    
    sim = TrisplitSimulation(initial_phase_errors = initial_phase_errors, v14_factor=v14_factor)
    sim.run_sim()
    return sim.bunch_length()

def run_sim():
    sim = TrisplitSimulation(initial_phase_errors = [0,0], v14_factor = 1.)
    sim.run_sim()
    

if __name__ == "__main__":
    start = perf_counter()
    bl = simulate_bunch_lengths(initial_phase_errors=[10,0], v14_factor=1.0)
    
    print(bl)
    #res = objective_function([0,0])
    #print('Result {}'.format(res))
    #test_one_run()
    #test_optimizer_lookup()
